# Description
You need to sign the finance_report.txt file with SignatureHelper.exe. Flag is mctf{md5sum(signature_of_finance_report)}.

# Writeup
Достаточно декомпильнуть таск в любом декомпиляторе (например, dnSpy) для .NET и изучить код. Будет видно, что вызов функции подписи на реальной точке входа происходит с аргументом, отличным от аргумента в вызове в методе Main. Далее сборка сохраняется в проект, убирается выкидывание ексепшна и спокойно получается нужная подпись.